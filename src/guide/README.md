# Introduction

This app is intended for *Online Documentation* of TC Software Applications
with the purpose of tracking project progress and also for the **Tech Support** to
find immediate ways to troubleshoot app concerns, if any.

Below are the available Projects that has Online Docs already.
Enjoy your stay.

## Projects
[UMID](/umid/)<br/>
[Youth Centralization](/online-membership/)<br/>
[SAP Concerns](/sap-concerns/)<br/>
