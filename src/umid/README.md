# UMID Introduction

This is UMID, A universalization tool by TC to segregate member accounts to an integral **Member Profile**.

## Database Design

This is the basic foundation of Database design in UMID. With this simple presentation a lot of things can be understood and be done from here.

	
@startuml
class _UmidHeader {
   +umid : VARCHAR
   otherColumns : []
}

class _UmidActiveBranch {
   +id : Integer
   -fk_umid: VARCHAR
   customer_id: VARCHAR
   status: VARCHAR
   branch_code: VARCHAR
}




package "Other Profile Data" #DDDDDD {
  class _UmidDependents {
   +DependentsID : Integer
   -fk_umid: VARCHAR
   otherColumns: []
}

class _UmidIdentificationDetails {
   +IndexId : Integer
   -fk_umid: VARCHAR
   otherColumns: []
}

class _UmidVehicles {
   +id : Integer
   -fk_umid: VARCHAR
   otherColumns: []
}
}

note left of _UmidActiveBranch::fk_umid
  This is foreign key to UMID.
  meaning a single UMID can
  have multiple CustomerID
end note

_UmidActiveBranch }-- _UmidHeader
_UmidDependents }-- _UmidHeader
_UmidIdentificationDetails }-- _UmidHeader
_UmidVehicles }-- _UmidHeader
@enduml


::: tip
You should also check **IsActive** column in_UMIDHeader to check if this profile is active or not!
:::