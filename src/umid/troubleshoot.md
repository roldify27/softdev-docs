# UMID FAQf

Here we will get to know the basic troubleshooting for UMID ambooot

::: warning Disclaimer
Of course we are trying to improve UMID facility to minimize if not get rid of all these concerns, but for the sake of testing this app to have better documentation/connection between the Soft. Dev. and Tech. Team, this procedures had been included here. Thank you.
:::


## Troubleshooting

### Naupdate na sa UMID pero wala pa ni-sync sa branch

#### Procedure 1
Check the connection to the concerned branch. Loss of connection causes the syncing/downloading of data to fail.


#### Procedure 2
First, run this SQL query against the UMID Database so we can check if there is really a pending update **to** be synced or downloaded in the Branch.

```sql
select * from _UMIDHeaderBranchUpdate where IsSync = 0
```

If there is, try to look for the very lengthy column values that normally happens like  ```Addresses (present and permanent```, ```Employment (Contact and Address)```, ```Business (Contact and Address)``` and sometimes the ```TransactedBy``` gets a little longer than 11 characters. This happens because in UMID, users are given a lengthy input field, but in ICAS, those field character count may already trigger a **truncated** error.
Just update those suspected fields in the **`_UMIDHeaderBranchUpdate`** table then restart the consolidator somewhere.

### Dili ma-sync ang profile gikan sa ICAS.

#### Procedure 1
The process of syncing/uploading data from ICAS to UMID uses `http://` API request under the hood.
This kind of API request uses the `&` character for passing the parameters to the API. Example:

```javascript
http://192.168.70.12?go-to-umid?name=harold&lastname=calio&middlename=malanog
```

what usually happens when an error occurs is that an `&` character is appended the url parameter, example instead of using `harold` as firstname, he used *`ha&rold`* instead. So what happens in our API is
```javascript
http://192.168.70.12?go-to-umid?name=ha&rold&lastname=calio&middlename=malanog

/** it breaks the url parameter and thus causes an error to the API.
A solution is to replace "&" charater with the word "and" instead,
This usually happens in business names and employment names i.g:
Buy & Sell business, Harold & Mcbell corp., etc. **/
```