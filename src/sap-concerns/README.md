# Introduction

An online handbook in handling *SAP-ICAS Journal Entry* Concerns.
> Before applying these workarounds, make sure you have seen the **error** in the Online ICAS-SAP Viewer, for some, if not most of the workarounds here are to be done in the said facility.

## List of common Errors

- **Invalid distribution rule**
    - **Cause:** New Branch was not assigned or created in SAP Facility.
    - **Source:** SAP
    - **Personnel In-Charge:** ==ICT== and ==Accounting Head==
    - **Solution:** SAP Admin(Accounting Manager/ICT Manager) will Create or declare the branch in the necessary fields in the ICAS-SAP system. 
-------
- **At least one amount is required in document rows**
    - **Cause:** The total debit and the total credit is equal to zero(0), in other words, the accounting entry amount is zero(0).
    - **Source:** SAP
    - **Personnel In-Charge:** ==Bookkeeper, or any accounting staff==
    - **Solution:** Make sure the Journal entry is not equal to zero(0), as SAP will not recognize it. Known Error.
-------
- **Invalid Account Code**
    - **Cause:** One or more SL Code/s in the Journal Entry is not yet existing in SAP.
    - **Source:** SAP
    - **Personnel In-Charge:** ==Bookkeeper, or any accounting staff==
    - **Solution:** Make sure to add the missing SL Code/s in SAP, then update the status of the Journal Entry in the Online ICAS-SAP Viewer Facility to PENDING, clear the error message and hit the UPDATE button.
                    ![alt text](https://live.staticflickr.com/65535/52030425423_e11e05bc32_z.jpg "Logo Title")
-------
- **JV Repeat**
    - **Cause:** The Journal Entry *reference number* is already been used or is already been entered to SAP.
    - **Source:** ICAS-SAP Middleware
    - **Personnel In-Charge:** ==Bookkeeper, or any accounting staff==
    - **Solution:** There are three possible ways to go about this.
        1. Change the reference number of Journal Entry in the Online ICAS-SAP Viewer Facility
        2. change its status to PENDING
        3. **Clear the error message** and click  the UPDATE button.
            ![alt text](https://live.staticflickr.com/65535/52030425423_e11e05bc32_z.jpg "Logo Title")
-------
- **Posting Period Locked**
    - **Cause:** This usually occurs specially on monthend where some entries are posted a day or days after of their posting date.
    - **Source:** SAP
    - **Personnel In-Charge:** ==ICT== and ==Accounting Head==
    - **Solution:** Request the Accounting Head to open or unlock a locked date in SAP. After everything is fixed, lock that date again *for security*.
-------
- **Unbalanced Transaction**
    - **Case 1**
        - **Cause:** Single Journal Entry from ICAS.
        - **Source:** SAP
        - **Personnel In-Charge:** ==ICT== 
        - **Solution:** Call Technical Support for the attachment and advice.
    - **Case 2**
        - **Cause:** Total debit is not the same with the total credit. This occurs when the amount/change deposited to the ATM account of the member after deduction of loan payment is over by a certain amount, usually 0.01 centavos due to rounding of numbers/decimals.
        - **Source:** SAP
        - **Personnel In-Charge:** ==Bookkeeper, or any accounting staff== 
        - **Solution:**
            1. Identify the member with excess change
                - Go to TW Transaction Facility
                - Paste tw posting reference on Reference no. Field
                - Click Enter then click print to view details of the batch posted
                - Compare the total amount paid to each loan and pledged to deposits if any against the pay column per member until the variance will be found
            2. ![alt text](https://live.staticflickr.com/65535/52053358934_2380b37bb4_z.jpg "Logo Title")
-------
- **You cannot edit totals in system currency**
    - **Cause:** A known error from SAP DB..
    - **Source:** SAP
    - **Personnel In-Charge:** ==Bookkeeper==
    - **Solution:** Click the Edit,  Clear the response field, update the status to Pending Then click the Update Button.
    ![alt text](https://live.staticflickr.com/65535/52030425423_e11e05bc32_z.jpg "Logo Title")

