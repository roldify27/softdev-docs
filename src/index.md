---
home: true
heroImage: https://v1.vuepress.vuejs.org/hero.png
tagline: We got you covered.
actionText: Quick Start →
actionLink: /guide/
features:
- title: Searchable Docs!
  details: Maybe we cannot yet go full paperless but how convenient it is to have a very easy searchable docs, aint it?
- title: No more painting diagrams!
  details: All charts/diagrams are just one code away!
- title: Tech. Support Friendly
  details: As this is tecnhincally made for Tech. people, our friends in the Tech. Support can now have firsthand idea on how to troubleshoot things! ❤️
footer: Made by Dev Team with ❤️
---
