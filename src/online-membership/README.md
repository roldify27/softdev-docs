# Introduction

## TC Youth Centralization

This document gives detailed description of the requirements for the Tagum Cooperative Youth Centralization. The purpose and scope of this document and the project background are also described below.

### **Purpose**

The purpose of this document is to give a detailed description of the requirements for the Tagum Cooperative Youth Centralization. In general this document prioritizes in writing the schema of the architecture that takes hold in this project. There are lots of abstractions that are needed to represent the objects and their behavior on the system in a more convenient way. Additionally, there are various figures that represent the described system that serve only for better understanding of the deployment.

**Revision History**
| Date | Version | Description   | Author          |
|------|---------|---------------|-----------------|
|      | 1.0.0.0 | Initial Draft | Mcbell S. Cadao |


**Document Approval**
| Date Approved | Approved by | Remarks                     | Signature |
|---------------|-------------|-----------------------------|-----------|
|               |             | Approved System Development |           |

::: tip
This is a tipf
:::