# Functional Requirements
In this section, you will see a list of use cases to be achieved by Tagum Cooperative Youth Centralization. This section begins from a use case diagram, which presents the use cases in a visualized form. Then, the description of use cases will be provided.

## Use Cases

	
@startuml
left to right direction
actor "Youth Program Coordinator/\nMembership" as User
actor "Teller/Cash Clerk" as Teller
actor "Member Care Assistant" as MSA
actor "Bookkeeper" as BKPR

rectangle ICAS {
  usecase "Membership Management" as (MemMgt)
  usecase "Tellering" as (Tellering)
  usecase "End/Beginning of Day" as (EOD)
  usecase "Backup Database" as (BackupDB)
  usecase "Accounting Management" as (ActgMgt)
}

User --> EOD
User --> BackupDB
Teller --> Tellering
Teller --> EOD
Teller --> BackupDB
Teller --> ActgMgt
MSA --> MemMgt
MSA --> Tellering
BKPR --> EOD
BKPR --> ActgMgt
@enduml

## Global Use Case Model

| Use Case              | Description                                                                                                                                                                                                                                                                                                                                                                |
|-----------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Membership Management | Comprises of create & update profile, create payment order and membership conversion.                                                                                                                                                                                                                                                                                      |
| Tellering             | Any form of transaction that involves cashiering falls on this operation. In addition, inter-branch facility on tellering already has passbook updating wherein a transacting member from other branch can now update its passbook on the branch he/she is transacting. Also, cancellation of deposit, withdrawal and close account are added up on inter-branch facility. |
| End/Beginning of Day  | Only performed when the accounting, tellering and cashiering are balanced. This will close the current banking date and opens the banking date for the next working day.                                                                                                                                                                                                   |
| **Database backup**       | Religiously keeps a database backup before and after end/beginning of day for recovery purposes in case the live database fails during operations.                                                                                                                                                                                                                         |
| Accounting Management | BATCM entry, BATDM entry, journal voucher and adjustment entry falls in this module.                                                                                                                                                                                                                                                                                       |
