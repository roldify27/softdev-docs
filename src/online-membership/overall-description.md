# Overall Description

## Product Perspective
This facility is a proprietary system. It is created to be used exclusively for Tagum Cooperative by its users. It is a facility added to the existing systems, i.e., FSComplement-iCAS that caters the override of the approval of the batch transactions. The following are its features:

- Post Batch Button in TW transaction. The button is enabled for all users but only the authorized user can perform the post batch.
- Update Button. Allows the authorized user to approve the Batch CM/DM transaction.
- Print Posted Button. Allows the authorized user to view the posted and unposted transaction/s before and after the approval.
- Delete Button. Enables the authorized user to delete the batch transaction.

## User Classes and Characteristics

Physical Actors:

 - **User** - The User is the one who will input all the transactions for Batch CM/DM.
 - **Bookkeeper/SOHead** - The Bookkeeper/SOHead is the one who is authorized to post batch TW transaction and approve the Batch CM/DM transaction.
