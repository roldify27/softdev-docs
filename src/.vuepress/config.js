const { description } = require('../../package')
module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'TC Dev Team Software Documentation',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }]
  ],

  markdown: {
    extendMarkdown: md => {
      md.use(require('markdown-it-plantuml'))
    },
  },

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: '',
    mdEnhance: {
      enableAll: true,
      tasklist: true,
    },
    darkmode: {
      'auto-switch': 'on'
    },
    editLinks: false,
    docsDir: '',
    editLinkText: '',
    lastUpdated: false,
    nav: [
      {
        text: 'Guide',
        link: '/guide/',
      },
      {
        text: 'Config',
        link: '/config/'
      },
      {
        text: 'VuePress',
        link: 'https://v1.vuepress.vuejs.org'
      }
    ],
    sidebar: {
      '/guide/': [
        {
          title: 'Guide',
          collapsable: false,
          children: [
            '',
            'using-vue',
          ]
        }
      ],
      '/online-membership/': [
        {
          title: 'TC Youth Centralization',
          collapsable: false,
          children: [
            '',
            'overall-description',
            'functional-requirements',
          ]
        }
      ],
      '/umid/': [
        {
          title: 'UMID',
          collapsable: false,
          children: [
            '',
            'troubleshoot'
          ]
        }
      ],
      '/sap-concerns/': [
        {
          title: 'SAP CONCERNS',
          collapsable: false,
          children: [
            '',
          ]
        }
      ],
    }
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    [
      'register-components',
      {
        componentsDir: '../components'
      }
    ],
    [
      "md-enhance",
      {
        // your options
      },
    ],
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
  ]
}
